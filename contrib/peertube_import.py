#!/usr/bin/env python

# SPDX-FileCopyrightText: Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
usage: peertube_import.py [-h] old_api old_user new_api new_user new_channel new_pass support

Import videos from one PeerTube to another

positional arguments:
  old_api      API URL of the old instance
  old_user     Username in the old instance
  new_api      API URL of the new instance
  new_user     Username in the new instance
  new_channel  Channel name in the new instance
  new_pass     Password for the user in the new instance
  support      Fallback support message for the new instance

optional arguments:
  -h, --help   show this help message and exit
"""

import argparse
import logging
import shutil
import tempfile

import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder

from typing import BinaryIO, List, NamedTuple


class Video(NamedTuple):
    "Intermediate representation of a Video."

    uuid: str
    name: str
    description: str
    support: str
    originally_published_at: str
    tags: List[str]
    download_url: str


class PeerTubeImport:
    """Import all videos from one PeerTube instance to another the hard way.
    This might be necessary if one instance cannot reach the other, but a third
    host, running this script, can reach both.
    """

    def __init__(
        self, old_api, old_user, new_api, new_user, new_channel, new_pass, support
    ):
        self._old_api = old_api
        self._old_user = old_user
        self._new_api = new_api
        self._new_user = new_user
        self._new_channel = new_channel
        self._new_pass = new_pass
        self._support = support

        self._lock_file = "./peertube_import.lock"

        self._init_logger()

        self._new_bearer = self._create_user_token()
        self._privacy_id = self._fetch_privacy_id()
        self._channel_id = self._fetch_user_channel_id()

        uuids = self._fetch_videos_uuids()
        for uuid in uuids:
            self._logger.debug(f"Starting to proceed on {uuid}")
            video = self._fetch_video_info(uuid)

            if self._lock_has_uuid(video.uuid):
                self._logger.info(f"Skipping {uuid} as it was already uploaded.")
                continue

            # At least on my machine, the default temp was some tmpfs - which is
            # too small for big videos.
            with tempfile.TemporaryFile(dir="/tmp/") as temp_file:
                file_type = None
                with requests.get(video.download_url, stream=True) as r:
                    file_type = r.headers["content-type"]
                    shutil.copyfileobj(r.raw, temp_file)
                self._logger.debug(f"Downloaded {uuid} locally")

                temp_file.seek(0)

                new_uuid = self._upload_video(video, temp_file, file_type)

                self._lock_add_uuids(video.uuid, new_uuid)
                self._logger.info(f"Uploaded {uuid} as {new_uuid}")

    def _init_logger(self):
        "Create and configure a simple logger"

        self._logger = logging.getLogger("peertube_import")

        logHandler = logging.StreamHandler()
        logHandler.setFormatter(
            logging.Formatter(
                fmt="[%(asctime)s] [%(levelname)-5s] %(message)s",
                datefmt="%Y-%m-%d %H:%M:%S",
            )
        )

        self._logger.addHandler(logHandler)
        self._logger.setLevel(logging.DEBUG)

    def _lock_has_uuid(self, uuid: str) -> bool:
        "Check if this old UUID was already used."

        try:
            with open(self._lock_file, "r") as f:
                for line in f.readlines():
                    if uuid in line:
                        return True
            return False
        except FileNotFoundError:
            return False

    def _lock_add_uuids(self, uuid_old: str, uuid_new: str):
        "Mark this old UUID as imported and add a mapping to the new one."

        with open(self._lock_file, "a") as f:
            f.write(f"{uuid_old},{uuid_new}\n")

    def _fetch_videos_uuids(self) -> List[str]:
        "Fetches a list of all video's UUIDs from the old instance."

        uuids = []
        while True:
            r = requests.get(
                f"{self._old_api}/accounts/{self._old_user}/videos?start={len(uuids)}&count=10"
            )
            resp = r.json()
            for video in resp["data"]:
                uuids.append(video["uuid"])
            if len(uuids) == resp["total"]:
                self._logger.info(f"Fetched {len(uuids)} UUIDs from the old instance")
                return uuids

    def _fetch_video_info(self, uuid: str) -> Video:
        "Fetches informationen and the best possible download link for a video from the old instance."

        r = requests.get(f"{self._old_api}/videos/{uuid}")
        resp = r.json()

        resolutions = {
            file["resolution"]["id"]: file["fileDownloadUrl"] for file in resp["files"]
        }
        best_res_url = resolutions[sorted(resolutions.keys())[-1]]

        # For whatever reasons, the full description is only available in a second
        # API resource and its link is relative to the root. Thus, the seven stripped
        # chars are "/api/v1"..
        r = requests.get(self._old_api + resp["descriptionPath"][7:])
        description = r.json()["description"]

        # Try to set a date similar to the one on the old instance.
        originally_published_at = None
        published_at_fields = [
            "originallyPublishedAt",
            "publishedAt",
            "createdAt",
            "updatedAt",
        ]

        for field in published_at_fields:
            if resp[field] is not None:
                originally_published_at = resp[field]
                break
        if not originally_published_at:
            raise ValueError(f"Video {uuid} has no usable date")

        support = resp["support"] if resp["support"] else self._support

        return Video(
            uuid,
            resp["name"],
            description,
            support,
            originally_published_at,
            resp["tags"],
            best_res_url,
        )

    def _create_user_token(self) -> str:
        "Create an OAuth2 token on the new instance, necessary for uploading videos."

        r = requests.get(f"{self._new_api}/oauth-clients/local")
        client = r.json()

        auth_payload = {
            "client_id": client["client_id"],
            "client_secret": client["client_secret"],
            "grant_type": "password",
            "password": self._new_pass,
            "username": self._new_user,
        }
        r = requests.post(f"{self._new_api}/users/token", data=auth_payload)
        r.raise_for_status()
        token = r.json()["access_token"]

        self._logger.info("Logged into new instance")

        return token

    def _fetch_privacy_id(self) -> int:
        "Fetch the public privacy identifier to be used on the new instance."

        r = requests.get(f"{self._new_api}/videos/privacies")
        for privacy_id, name in r.json().items():
            if name == "Public":
                return privacy_id
        raise AttributeError("No public privacy identifier was found")

    def _fetch_user_channel_id(self) -> int:
        "Fetch the channel_id for the user's channel on the new instance."

        r = requests.get(f"{self._new_api}/video-channels/{self._new_channel}")
        resp = r.json()
        return resp["id"]

    def _upload_video(self, video: Video, temp_file: BinaryIO, file_type: str) -> str:
        "Upload a video to the new instance and returns the new UUID."

        tags = {
            f"tags[{no}]": tag for no, tag in zip(range(len(video.tags)), video.tags)
        }
        m = MultipartEncoder(
            fields={
                "channelId": str(self._channel_id),
                "name": video.name,
                "videofile": ("f", temp_file, file_type),
                "description": video.description,
                "originallyPublishedAt": video.originally_published_at,
                "privacy": str(self._privacy_id),
                "support": video.support,
                **tags,
            }
        )

        r = requests.post(
            f"{self._new_api}/videos/upload",
            data=m,
            headers={
                "Content-Type": m.content_type,
                "Authorization": f"Bearer {self._new_bearer}",
            },
        )
        r.raise_for_status()
        return r.json()["video"]["uuid"]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Import videos from one PeerTube to another"
    )
    parser.add_argument("old_api", help="API URL of the old instance")
    parser.add_argument("old_user", help="Username in the old instance")
    parser.add_argument("new_api", help="API URL of the new instance")
    parser.add_argument("new_user", help="Username in the new instance")
    parser.add_argument("new_channel", help="Channel name in the new instance")
    parser.add_argument("new_pass", help="Password for the user in the new instance")
    parser.add_argument("support", help="Fallback support message for the new instance")
    args = parser.parse_args()

    PeerTubeImport(
        args.old_api,
        args.old_user,
        args.new_api,
        args.new_user,
        args.new_channel,
        args.new_pass,
        args.support,
    )
