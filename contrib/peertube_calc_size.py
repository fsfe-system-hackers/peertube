#!/usr/bin/env python

# SPDX-FileCopyrightText: Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
usage: peertube_calc_size.py [-h] api user

Calculate sizes for a user's videos

positional arguments:
  api         API URL of the instance
  user        Username in the instance

optional arguments:
  -h, --help  show this help message and exit
"""

import argparse
import requests

from datetime import datetime, timezone
from typing import Dict, List


def fetch_videos(api_url: str, user: str) -> List[str]:
    "Fetches a list of all video's UUIDs from the instance."

    uuids = []
    while True:
        r = requests.get(
            f"{api_url}/accounts/{user}/videos?start={len(uuids)}&count=10"
        )
        resp = r.json()
        for video in resp["data"]:
            uuids.append(video["uuid"])
        if len(uuids) == resp["total"]:
            return uuids


def video_metrics(api_url: str, uuid: str) -> Dict[str, int]:
    "Create a dict of storage and traffic metrics for this video."

    r = requests.get(f"{api_url}/videos/{uuid}")
    resp = r.json()
    resolutions = {file["resolution"]["id"]: file["size"] for file in resp["files"]}

    best_key = sorted(resolutions.keys())[-1]
    best = resolutions[best_key]

    total = sum(resolutions.values())

    views = resp["views"]
    created_at = datetime.fromisoformat(resp["createdAt"][:-1]).astimezone(timezone.utc)
    traffic = (best * views) / ((datetime.now(timezone.utc) - created_at).days / 30)

    return {
        "best": best,
        "total": total,
        "traffic": traffic,
    }


def pprint_bytes(b: int) -> str:
    "Return human-readable format for sizes."

    for unit in ["", "Ki", "Mi", "Gi", "Ti"]:
        if b < 1024:
            return f"{b:.2f}{unit}B"
        b /= 1024.0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Calculate sizes for a user's videos")
    parser.add_argument("api", help="API URL of the instance")
    parser.add_argument("user", help="Username in the instance")
    args = parser.parse_args()

    uuids = fetch_videos(args.api, args.user)
    files = [video_metrics(args.api, uuid) for uuid in uuids]

    files_best = sum(file["best"] for file in files)
    files_total = sum(file["total"] for file in files)
    files_traffic = sum(file["traffic"] for file in files)

    print(f"There are {len(files)} videos")
    print(f"Total size in best format is {pprint_bytes(files_best)}")
    print(f"Total size in all formats is {pprint_bytes(files_total)}")
    print(f"Average monthly traffic is {pprint_bytes(files_traffic)}")
