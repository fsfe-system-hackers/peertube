#!/usr/bin/env bash

# SPDX-FileCopyrightText: Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# This script transfers the peertube_import.lock file created by the
# peertube_import.py script to a CSV of the old instance's URL mapping to new
# URL. However, this script is HIGHLY opinionated as our old instance used URLs
# based on the full UUID while our new one uses short UUIDs.
#
# Next to the core utils, curl and jq are required.

set -euE

LOCKFILE="./peertube_import.lock"

PT_OLD="https://peertube.social"
PT_NEW="https://media.fsfe.org"


# pt_short_uiid maps an UUID on the new PeerTube instance to the short UUID.
function pt_short_uiid() {
  curl -s "${PT_NEW}/api/v1/videos/${1}" | jq -r ".shortUUID"
}

# make_url_map takes an input CSV line and creates an output CSV line.
function make_url_map() {
  local old_uuid new_uuid
  old_uuid="$(cut -d ',' -f 1 <<< "$1")"
  new_uuid="$(cut -d ',' -f 2 <<< "$1")"

  echo "${PT_OLD}/videos/watch/${old_uuid},${PT_NEW}/w/$(pt_short_uiid "$new_uuid")"
}


while read -r line; do
  make_url_map "$line"

  # The API is rate limited and likes to start throwing 429 errors.
  sleep 1
done < "$LOCKFILE"
