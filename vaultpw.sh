#!/bin/sh

# SPDX-FileCopyrightText: Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

gpg --batch --use-agent --decrypt ./vaultpw.gpg
