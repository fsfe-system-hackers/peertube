#!/usr/bin/env python

# SPDX-FileCopyrightText: Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import argparse
import csv
import pathlib
import random
import requests
import time

from typing import List, NamedTuple


class FollowerData(NamedTuple):
    time: int
    kind: str
    name: str
    owner: str
    followers: int


class Account:
    def __init__(self, api: str, name: str):
        self._api = api
        self._name = name

    def _fetch_api(self, endpoint: str) -> dict:
        # Add a small delay against the rate limiting..
        time.sleep(0.1 * random.randint(0, 15))

        r = requests.get(f"{self._api}/{endpoint}")
        r.raise_for_status()
        return r.json()

    @property
    def followers(self) -> FollowerData:
        resp = self._fetch_api(f"accounts/{self._name}")
        return FollowerData(int(time.time()), "account", self._name, self._name, resp["followersCount"])

    @property
    def channels(self) -> List[FollowerData]:
        resp = self._fetch_api(f"accounts/{self._name}/video-channels")
        return [
            FollowerData(
                int(time.time()),
                "channel",
                ch["name"],
                self._name,
                ch["followersCount"],
            )
            for ch in resp["data"]
        ]

    @property
    def videos(self) -> List[FollowerData]:
        data = []
        total = float("inf")

        while len(data) < total:
            resp = self._fetch_api(f"accounts/{self._name}/videos?start={len(data)}")

            total = resp["total"]
            data += [FollowerData(int(time.time()), "video", d["name"], self._name, d["views"]) for d in resp["data"]]

        return data

    @property
    def all_followers(self) -> List[FollowerData]:
        return [self.followers] + self.channels + self.videos


def write_csv(api: str, csv_path: str, accounts: List[str]):
    write_header = not pathlib.Path(csv_path).is_file()

    with open(csv_path, "a", newline="") as f:
        writer = csv.DictWriter(f, fieldnames=FollowerData.__annotations__.keys())

        if write_header:
            writer.writeheader()

        for account in accounts:
            writer.writerows([fd._asdict() for fd in Account(api, account).all_followers])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="PeerTube follower stats")
    parser.add_argument(
        "--api",
        default="https://media.fsfe.org/api/v1",
        help="API URL of the PeerTube instance.",
    )
    parser.add_argument("csv", help="CSV file to be amended.")
    parser.add_argument("account", nargs="+", help="Account name, might be specified multiple times.")
    args = parser.parse_args()

    write_csv(args.api, args.csv, args.account)
